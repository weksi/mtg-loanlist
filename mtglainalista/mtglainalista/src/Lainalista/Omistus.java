/**
 * 
 */
package Lainalista;

import java.io.OutputStream;
import java.io.PrintStream;

import fi.jyu.mit.ohj2.Mjonot;
import kanta.Apuja;

/**
 * - Tiet�� omistajan kent�t(hloId,id, m��r�,foilm��r�)                                                
 * - osaa tarkistaa tietyn kent�n oikeellisuuden      
 * - osaaa muuttaa |2|1| merkkijonon omistajan tiedoksi   
 * 
 * @author Mika Koskela, Ville-Veikko V�h�aho
 * @version 1.0, 25.3.2014
 *
 */
public class Omistus {

    private int tunnusNro;
    private int id;
    private int hloId;
    private int maara;
    private int foilMaara;

    private static int seuraavaNro = 1;

    /**
     * @return ensimm�isen attribuutin kentt�, jota on tarkoitus k�sitell�
     */
    public int ekaKentta() {
	return 2;
    }

    /**
     * @return attribuuttien m��r�
     */
    public int getKenttia() {
	return 5;
    }

    /**
     * Oletusmuodostaja, jonka ei tarvi viel� tehd� mit��n ja on toistaiseksi vain testaamisen vuoksi mukana
     */
    public Omistus() {
	//
    }

    /**
     * Muodostaja, jossa tarvitaan henkil�n ja kortin tunnukset ja korttien m��r�, mutta foileja ei tiedet�
     * Kutsutaan muodostajaa, miss� foilien m��r� on 0
     * @param hloId henkil�n tunnusNro
     * @param id kortin tunnusNro
     * @param maara korttien m��r�
     */
    public Omistus(int hloId, int id, int maara) {
	this(hloId, id, maara, 0);
    }

    /**
     * Muodostaja, miss� tiedet��n "kaikki tiedot"
     * @param hloId henkil�n tunnusNro
     * @param id kortin tunnusNro
     * @param maara korttien m��r�
     * @param foilMaara foilkorttien m��r�
     */
    public Omistus(int hloId, int id, int maara, int foilMaara) {
	this.hloId = hloId;
	this.id = id;
	this.maara = maara;
	this.foilMaara = foilMaara;
    }

    /**
     * rekister�i kortin (kopioitu vesan mallista)
     * @return rekister�idyn kortin tunnusnro
     * @example <pre name="test">
     *   Omistus eka = new Omistus(), toka = new Omistus();
     *   eka.rekisteroi(); eka.getTunnusNro() === 1; 
     *   toka.rekisteroi();
     *   eka.getTunnusNro() === (toka.getTunnusNro() - 1);
     * </pre>
     */
    public int rekisteroi() {
	tunnusNro = seuraavaNro;
	seuraavaNro++;
	return tunnusNro;
    }

    /**
     * @return Omistuksen oma tunnusnumero
     */
    public int getTunnusNro() {
	return this.tunnusNro;
    }

    /**
     * @return palautetaan kyseisten korttien omistajalla oleva m��r�
     */
    public int getMaara() {
	return maara;
    }

    /**
     * @return palautetaan kyseisten kiiltokuva-korttien omistajalla oleva m��r�
     */
    public int getFoilMaara() {
	return foilMaara;
    }

    /**
     * @return henkil�n tunnusnumero
     */
    public int getHloId() {
	return hloId;
    }

    /**
     * @return kortin tunnusnumero
     */
    public int getId() {
	return id;
    }

    /**
     * Tulostetaan omistuksen tiedot
     * @param out tietovirta minne tulostetaan
     */
    public void tulosta(PrintStream out) {
	out.print(String.format("%03d", tunnusNro) + " ");
	if (foilMaara == 0)
	    out.println("Henkil� " + hloId + " omistaa " + maara + " kpl \nkorttia " + id);
	else
	    out.println("Henkil� " + hloId + " omistaa " + maara + " kpl tavallista ja "
		    + foilMaara + " foil \nkorttia " + id);
    }

    /**
     * Tulostetaan omistuksen tiedot
     * @param os tietovirta minne tulostetaan
     */
    public void tulosta(OutputStream os) {
	tulosta(new PrintStream(os));
    }

    /**
     * 
     * @param nr
     */
    private void setHloId(int nr) {
	hloId = nr;
    }

    /**
     * 
     * @param nr
     */
    private void setId(int nr) {
	id = nr;
    }

    private void setTunnusNro(int nro) {
	tunnusNro = nro;
	if (tunnusNro > seuraavaNro)
	    seuraavaNro = tunnusNro + 1;
    }

    /**
     * Apumetodi testausta varten, miss� annetaan ennalta m��ritetyt arvot omistukselle
     * @param nro omistanjanId
     */
    public void vastaaAnttiCavern(int nro) {
	hloId = nro;
	id = (int) Apuja.rand(1, 10);
	maara = 4;
	foilMaara = (int) Apuja.rand(10, 80);
    }

    /**
     * 
     */
    @Override
    public String toString() {
	return "" + getTunnusNro() + "|" + getHloId() + "|" + getId() + "|" + getMaara() + "|"
		+ getFoilMaara();
    }

    /**
     * @param rivi rivi mist� olion tiedot poimitaan
     */
    public void parse(String rivi) {
	StringBuffer sb = new StringBuffer(rivi);
	setTunnusNro(Mjonot.erota(sb, '|', getTunnusNro()));
	setHloId(Mjonot.erota(sb, '|', getHloId()));
	setId(Mjonot.erota(sb, '|', getId()));
	maara = Mjonot.erota(sb, '|', getMaara());
	foilMaara = Mjonot.erota(sb, '|', getFoilMaara());
    }

    /**
     * Testiohjelma Omistukselle
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	Omistus jokin = new Omistus();
	jokin.vastaaAnttiCavern(1);
	jokin.tulosta(System.out);
	jokin.rekisteroi();
	jokin.tulosta(System.out);
    }

    /**
     * palautetaan haluttu attribuutti
     * @param k kytkimen numero
     * @return halutun attribuutin arvo
     */
    public int anna(int k) {
	switch (k) {
	case 0:
	    return tunnusNro;
	case 1:
	    return hloId;
	case 2:
	    return id;
	case 3:
	    return maara;
	case 4:
	    return foilMaara;
	default:
	    return -1;
	}
    }

    /**
     * Lis�t��n henkil�lle saman tyypin kortteja
     * @param lisattavaMaara henkil�lle lis�tt�vien korttien m��r�
     */
    public void lisaaKortteja(int lisattavaMaara) {
	maara += lisattavaMaara;
    }

}