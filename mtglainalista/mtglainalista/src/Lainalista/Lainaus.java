/**
 * 
 */
package Lainalista;

import java.io.PrintStream;

import kanta.Apuja;

import fi.jyu.mit.ohj2.Mjonot;

/**
 * Tiet�� haltijan kent�t(hloid, id, m��r�, vanhahlo|id)                                                 
 * osaa tarkistaa tietyn kent�n oikeellisuuden                   
 * osaaa muuttaa |2| merkkijonon lainaajan                       
 * tiedoksi                                         
 * @author Ville-Veikko V�h�aho ja Mika Koskela
 * @version 26.2.2014
 */
public class Lainaus {

    private int tunnusNro;
    private int korttiId = 0;
    private int omistajaId = 0;
    private int haltijaId = 0;
    private int kpl = 0;

    private static int seuraavaNro = 1;

    /**
     * ei tee viel� mit��n. Mukana vain testauksen vuoksi
     */
    public Lainaus() {
	//
    }

    /**
     * 
     * @param haltijaId haltijan tunnusnumero
     * @param korttiId kortin tunnusnumero
     * @param kpl haltijan korttien m��r�
     * @param omistajaId kortin omistajan id
     */
    public Lainaus(int haltijaId, int korttiId, int kpl, int omistajaId) {
	this.haltijaId = haltijaId;
	this.korttiId = korttiId;
	this.kpl = kpl;
	this.omistajaId = omistajaId;
    }

    /**
    * @return Lainauksen oma tunnusnumero
    */
    public int getTunnusNro() {
	return this.tunnusNro;
    }

    /**
     * @return kortin tunnusnumero
     */
    public int getKorttiId() {
	return this.korttiId;
    }

    /**
     * @return palautetaan kyseisten korttien haltijalla oleva m��r�
     */
    public int getKpl() {
	return kpl;
    }

    /**
     * @return haltijan tunnusnumero
     */
    public int getHaltijaId() {
	return haltijaId;
    }

    /**
     * @return omistajan tunnusnumero
     */
    public int getOmistajaId() {
	return omistajaId;
    }

    /**
     * @return lainauksen tunnusnumero
     * @example
     * <pre name="test">
     * Lainaus eka = new Lainaus();
     * Lainaus toka = new Lainaus();
     * eka.rekisteroi(); 
     * eka.getTunnusNro() === 1; 
     * toka.rekisteroi();
     * eka.getTunnusNro() === (toka.getTunnusNro() - 1);
     * </pre>
     */
    public int rekisteroi() {
	tunnusNro = seuraavaNro;
	seuraavaNro++;
	return tunnusNro;
    }

    /**
    * Tulostetaan lainauksen tiedot
      * @param out tietovirta minne tulostetaan
      */
    public void tulosta(PrintStream out) {
	out.print(String.format("%03d", tunnusNro) + " ");
	out.println("haltija " + haltijaId + " omistaa " + kpl + " kpl korttia " + korttiId
		+ " jotka omistaa " + omistajaId);
    }

    /**
    * Apumetodi testausta varten, miss� annetaan ennalta m��ritetyt arvot lainaukselle
    * @param nro haltijanId
    */
    public void vastaaApuLainaus(int nro) {
	haltijaId = (int) Apuja.rand(0, 3);
	korttiId = (int) Apuja.rand(1, 8);
	kpl = 2;
	omistajaId = nro;
    }

    /**
     * 
     * @param nr
     */
    private void setKorttiId(int nr) {
	korttiId = nr;
	// if (korttiId > seuraavaNro) seuraavaNro = korttiId + 1;
    }

    /**
     * 
     * @param nr
     */
    private void setOmistajaId(int nr) {
	omistajaId = nr;
	// if (omistajaId >= seuraavaNro) seuraavaNro = omistajaId + 1;
    }

    /**
     * 
     * @param nr
     */
    private void setHaltijaId(int nr) {
	haltijaId = nr;
	// if (haltijaId >= seuraavaNro) seuraavaNro = haltijaId + 1;
    }

    @Override
    public String toString() {
	return "" + getHaltijaId() + "|" + getKorttiId() + "|" + kpl + "|" + getOmistajaId();
    }

    /**
     * 
     * @param rivi rivi mist� tiedot poimitaan
     */
    public void parse(String rivi) {
	StringBuffer sb = new StringBuffer(rivi);
	setHaltijaId(Mjonot.erota(sb, '|', getHaltijaId()));
	setKorttiId(Mjonot.erota(sb, '|', getKorttiId()));
	kpl = Mjonot.erota(sb, '|', kpl);
	setOmistajaId(Mjonot.erota(sb, '|', getOmistajaId()));
    }

    /**
     * Testiohjelma Omistukselle
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	Lainaus laina = new Lainaus();
	laina.vastaaApuLainaus(1);
	laina.tulosta(System.out);
	laina.rekisteroi();
	laina.tulosta(System.out);

    }

}
