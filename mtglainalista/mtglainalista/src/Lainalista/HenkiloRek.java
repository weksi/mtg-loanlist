package Lainalista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import fi.jyu.mit.ohj2.WildChars;

/**
 * - osaa lis�t� ja poistaa henkil�n              
 * - lukee ja kirjoittaa henkil�rek tiedostoon    
 * - osaa etsi� ja lajitella 
 * @author Ville-Veikko V�h�aho ja Mika Koskela
 * @version 1.0, 8.3.2014
 * 
 */
public class HenkiloRek {
    private static final int MAX_HENKILOA = 6;
    private int henkiloidenLkm = 0;
    private String tiedostonNimi = "henkilot";
    private Henkilo alkiot[] = new Henkilo[MAX_HENKILOA];
    private boolean muutettu = false;

    /**
     * oletus muodostaja
     */
    public HenkiloRek() {

    }

    /**
     * Lis�� uuden henkil�n tietorakenteeseen.
     * @param henkilo lis�tt�v�n j�senen viite. tietorakenne muuttuu
     * omistajaksi.
     * @throws SailoException jos tietorakenteeseen ei mahdu enemp�� alkioita
     * @example
     * <pre name="test">
     * #THROWS SailoException 
     * HenkiloRek henkilot = new HenkiloRek(); 
     * Henkilo weksi = new Henkilo(); 
     * Henkilo mika = new Henkilo(); 
     * henkilot.getLkm() === 0; 
     * henkilot.lisaa(mika); henkilot.getLkm()  === 1; 
     * henkilot.lisaa(weksi); henkilot.getLkm() === 2; 
     * henkilot.lisaa(weksi); henkilot.getLkm() === 3; 
     * henkilot.anna(0) === mika; 
     * henkilot.anna(1) === weksi; 
     * henkilot.anna(2) === weksi; 
     * henkilot.anna(1) == mika === false; 
     * henkilot.anna(0) == mika === true; 
     * henkilot.anna(1) == weksi === true; 
     * henkilot.anna(3) === weksi; #THROWS IndexOutOfBoundsException     
     * henkilot.lisaa(weksi); henkilot.getLkm() === 4; 
     * henkilot.lisaa(mika); henkilot.getLkm()  === 5; 
     * henkilot.lisaa(weksi); henkilot.getLkm() === 6; 
     * henkilot.lisaa(mika); henkilot.getLkm(); #THROWS SailoException
     * </pre>
     */
    public void lisaa(Henkilo henkilo) throws SailoException {
	if (henkiloidenLkm >= alkiot.length)
	    alkiot = kasvataTaulukko();
	alkiot[henkiloidenLkm] = henkilo;
	henkiloidenLkm++;
	muutettu = true;

    }

    /**
     * Kokeillaan l�ytyyk� annetulla id:ll� henkil�it�
     * @param id henkil�n id
     * @return -1 jos ei l�ydy, muuten henkil�n tunnusNro
     */
    public int etsiId(int id) {
	for (int i = 0; i < henkiloidenLkm; i++)
	    if (id == alkiot[i].getHloid())
		return i;
	return -1;
    }

    /**
     * Kasvatetaan pieneksi k�ynytt� taulukkoa 4:ll�
     * @return kasvatettu taulukko
     */
    private Henkilo[] kasvataTaulukko() {
	int kasvatusMaara = 4;

	Henkilo[] uusi = new Henkilo[alkiot.length + kasvatusMaara];
	for (int i = 0; i < alkiot.length; i++) {
	    uusi[i] = alkiot[i];
	}

	return uusi;
    }

    /**
     * palauttaa viitteen henkil�n paikasta i
     * @param i mink� paikan henkil� halutaan
     * @return viite henkil��n, jonka indeksi on i
     * @throws IndexOutOfBoundsException jos i ei ole sallitulla alueella
     */
    public Henkilo anna(int i) throws IndexOutOfBoundsException {
	for (Henkilo henkilo : alkiot) {
	    if (henkilo == null)
		continue;
	    if (henkilo.getHloid() == i)
		return henkilo;
	}
	if (i < 0 || henkiloidenLkm <= i)
	    throw new IndexOutOfBoundsException("Laitonindeksi: " + i);
	// return alkiot[i];

	return null;
    }

    /**
     * Palauttaa henkil� rekisterin henkil�iden lukum��r�n
     * @return henkil�iden lukum��r�
     */
    public int getLkm() {
	return henkiloidenLkm;
    }

    /**
     * Etsit��n annetuissa rajoissa henkil�it�
     * @param haku hakukent�st� saatu merkkijono
     * @param numero henkil�iden lkm
     * @return lista henkil�ist� jotka t�ytt�� hakuehdon
     */
    public List<Henkilo> etsi(String haku, int numero) {
	List<Henkilo> loytyneet = new ArrayList<Henkilo>();
	for (int i = 0; i < numero; i++) {
	    Henkilo hen = alkiot[i];
	    String j = hen.getNimi();
	    if (WildChars.onkoSamat(j, haku))
		loytyneet.add(hen);
	}
	return loytyneet;
    }

    /**
     * Lukee henkil� tiedostoa. T�m� osa viel� kesken.
     * @param hakemisto tiedoston hakemisto
     * @throws SailoException jos lukeminen ep� onnistuu
     * 
     */
    public void lueTiedostosta(String hakemisto) throws SailoException {
	try (BufferedReader fi = new BufferedReader(new FileReader("henkilot.dat"))) {
	    setTiedostonNimi(hakemisto);
	    String rivi = null;

	    while (((rivi = fi.readLine()) != null)) {
		rivi = rivi.trim();
		Henkilo henkilo = new Henkilo();
		henkilo.parse(rivi);
		lisaa(henkilo);
	    }
	    muutettu = false;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

    /**
     * Tallentaa henkil� tiedostoon. T�m� osa on viel� kesken.
     * @throws SailoException jos tallennus ep�onnistuu
     */
    public void tallenna() throws SailoException {
	if (!muutettu)
	    return;
	File ftied = new File("henkilot.dat");
	try (PrintWriter fo = new PrintWriter(new FileWriter(ftied.getCanonicalPath()))) {
	    for (Henkilo henkilo : alkiot) {
		if (henkilo != null) fo.println(henkilo.toString());
	    }
	    /*for (int i = 0; i < getLkm(); i++) {
		Henkilo henkilo = anna(i);
		if (henkilo == null)
		    continue;
		fo.println(henkilo.toString());
	    }*/
	} catch (IOException e) {
	    e.printStackTrace();
	}
	muutettu = false;
    }

    /**
     * 
     * @param nimi tiedoston nimi
     */
    public void setTiedostonNimi(String nimi) {
	tiedostonNimi = nimi;
    }

    /**
     * @return tiedoston nimi
     */
    public String getTiedostonNimi() {
	return tiedostonNimi + ".dat";
    }

    /**
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	HenkiloRek henkilot = new HenkiloRek();

	Henkilo weksi = new Henkilo();
	weksi.rekisteroi();
	weksi.vastaaHenkilo2();
	Henkilo mika = new Henkilo();
	mika.rekisteroi();
	mika.vastaaHenkilo();

	try {
	    henkilot.lisaa(weksi);
	    henkilot.lisaa(mika);

	    System.out.println("Henkil� rekisteri testi");

	    for (int i = 0; i < henkilot.getLkm(); i++) {
		Henkilo henkilo = henkilot.anna(i);
		System.out.println("henkil� nro: " + i);
		henkilo.tulosta(System.out);
	    }
	} catch (SailoException ex) {
	    System.out.println(ex.getMessage());
	}

    }

    /**
     * Poistetaan annetun indeksin mukainen henkil�
     * @param id poistettavan henkil�n indeksi
     * @return 0 jos ei onnistunut ja 1 jos onnistui
     */
    public int poista(int id) {
	int ind = etsiId(id);
	if (ind < 0)
	    return 0;
	henkiloidenLkm--;
	for (int i = ind; i < henkiloidenLkm; i++)
	    alkiot[i] = alkiot[i + 1];
	alkiot[henkiloidenLkm] = null;
	muutettu = true;
	return 1;
    }

    /**
     * @return lista miss� on kaikki henkil�t
     */
    public Henkilo[] annaKaikki() {
	return alkiot;
    }
}
