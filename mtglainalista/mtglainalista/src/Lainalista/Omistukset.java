/**
 * 
 */
package Lainalista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * - osaa lis�t� ja v�hent�� henkil�n omistamien korttien ja foiled korttien m��r�              
 * - lukee ja kirjoittaa henkil�rek tiedostoon       
 * - osaa etsi� ja lajitella
 * 
 * Luokka tehty Vesa Lappalaisen mallin pohjalta
 * 
 * @author Mika Koskela, Ville-Veikko V�h�aho
 * @version 1.0, 25.3.2014
 *
 */
public class Omistukset {

    private String tiedostonNimi = "";
    private final List<Omistus> omistukset = new ArrayList<Omistus>();
    private boolean muutettu = false;
    private int lkm = 0;

    /**
     * Oletusmuodostaja, jossa ei viel� tehd� mit��n
     */
    public Omistukset() {
	// viel� ei mit��n
    }

    /**
     * Haetaan kaikki yhden henkil�n omistamat kortit
     * @param hloId Omistajan tunnusNro
     * @return tietorakenne, jossa viitteet l�ydettyihin harrastuksiin
     * @example
     * <pre name="test">
     * #import java.util.*;
     * 
     *  Omistukset omistukset = new Omistukset();
     *  Omistus eka = new Omistus(); 
     *  Omistus toka = new   Omistus(); 
     *  Omistus kolmas = new Omistus(); 
     *  Omistus neljas = new Omistus(); 
     *  Omistus viides = new Omistus();
     *  eka.vastaaAnttiCavern(1);
     *  toka.vastaaAnttiCavern(4);
     *  kolmas.vastaaAnttiCavern(4);
     *  neljas.vastaaAnttiCavern(4);
     *  viides.vastaaAnttiCavern(3);
     *  omistukset.lisaa(eka);        
     *  omistukset.lisaa(toka);      
     *  omistukset.lisaa(kolmas);  
     *  omistukset.lisaa(neljas); 
     *  omistukset.lisaa(viides);  
     *  
     *  List<Omistus> loytyneet;
     *  loytyneet = omistukset.annaOmistukset(1);
     *  loytyneet.size() === 1;
     *  loytyneet = omistukset.annaOmistukset(4);
     *  loytyneet.size() === 3;
     *  loytyneet.get(0) == toka === true;
     *  loytyneet.get(1) == kolmas === true;
     *  loytyneet.get(2) == neljas === true;
     *  loytyneet = omistukset.annaOmistukset(3);
     *  loytyneet.size() === 1;
     *  loytyneet.get(0) == viides === true;
     *  loytyneet.get(0) == toka === false;
     * </pre>
     */
    public List<Omistus> annaOmistukset(int hloId) {
	List<Omistus> loydetyt = new ArrayList<Omistus>();
	for (Omistus omistus : omistukset) {
	    if (omistus.getHloId() == hloId)
		loydetyt.add(omistus);
	}
	return loydetyt;
    }

    /**
     * Annetaan omistukset, miss� joku omistaa kyseist� korttia
     * @param tunnusNro kortin tunnusNro
     * @return lista omistuksista
     */
    public List<Omistus> annaOmistuksetKortilla(int tunnusNro) {
	List<Omistus> loydetyt = new ArrayList<Omistus>();
	for (Omistus omistus : omistukset) {
	    if (omistus.getId() == tunnusNro)
		loydetyt.add(omistus);
	}
	return loydetyt;
    }

    /**
     * Lis�t��n uusi omistus tietorakenteeseen
     * @param omistus Omistus-olio
     */
    public void lisaa(Omistus omistus) {
	omistukset.add(omistus);
	lkm++;
	muutettu = true;
    }

    /**
     * 
     * @return omistuksessa olevien korttien lukum��r�
     */
    public int getLkm() {
	return lkm;
    }

    /**
     * Luetaan tiedostosta tiedot, mutta ei osata viel�
     * @param tiedNimi tiedostonnimi, mist� luetaan
     * @throws SailoException poikkeus, jos tiedoston lukeminen ei onnistu
     */
    public void lueTiedosto(String tiedNimi) throws SailoException {

	// BufferedReader fi;
	try (BufferedReader fi = new BufferedReader(new FileReader("omistus.dat"))) {
	    // fi = new BufferedReader(new FileReader("lainaus.dat"));
	    String rivi;
	    while ((rivi = fi.readLine()) != null) {
		rivi = rivi.trim();
		Omistus omistus = new Omistus();
		omistus.parse(rivi);
		lisaa(omistus);
	    }
	    muutettu = false;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Talletetaan tiedostoon, mutta viel� ei osata
     * @throws SailoException jos tallentaminen ei onnistu niin heitet��n virhe
     */
    public void talleta() throws SailoException {
	if (!muutettu)
	    return;
	File ftied = new File("omistus.dat");
	try (PrintWriter fo = new PrintWriter(new FileWriter(ftied.getCanonicalPath()))) {
	    for (Omistus omistus : omistukset) {
		fo.println(omistus.toString());
	    }

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	Omistukset lista = new Omistukset();
	Omistus eka = new Omistus(), toka = new Omistus();
	eka.vastaaAnttiCavern(1);
	toka.vastaaAnttiCavern(2);
	lista.lisaa(eka);
	eka.rekisteroi();
	toka.rekisteroi();
	lista.lisaa(toka);

	List<Omistus> lista2 = lista.annaOmistukset(1);
	for (Omistus omistus : lista2) {
	    omistus.tulosta(System.out);
	}
    }

    /**
     * Tarkastetaan, onko annetulla henkil�ll� jo kyseist� korttia omistuksessa
     * @param hloid omistajan tunnusnro
     * @param tunnusNro kortin tunnusnro
     * @return null jos ei l�ydy kyseist� korttia, muuten annetaan omistus miss� kortti on mukana 
     */
    public Omistus tarkastaOmistukset(int hloid, int tunnusNro) {
	for (Omistus omistus : omistukset) {
	    if (omistus.getHloId() == hloid && omistus.getId() == tunnusNro)
		return omistus;
	}
	return null;
    }

    /** 
     * poistetaan annetut omistukset
     * @param poistettava lista poistettavista omistuksista
     * @return j�ljell� olevat omistukset
     */
    public Object poista(List<Omistus> poistettava) {

	for (int a = 0; a < omistukset.size(); a++) {
	    int o = omistukset.get(a).getTunnusNro();
	    for (int i = 0; i < poistettava.size(); i++) {
		int e = poistettava.get(i).getTunnusNro();
		if (e == o) {
		    omistukset.remove(a);
		    a--;
		}
	    }
	}
	muutettu = true;
	return omistukset;
    }

    /**
     * Poistetaan annettu omistus
     * @param poistettava poistettava omistus-olio
     */
    public void poista(Omistus poistettava) {
	omistukset.remove(poistettava);
	muutettu = true;

    }

}
