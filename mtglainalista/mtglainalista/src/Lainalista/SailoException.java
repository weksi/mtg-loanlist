/**
 * 
 */
package Lainalista;

/**
 * Poikkeusluokka tietorakenteesta aiheutuville poikkeuksille, tehty mallin mukaan
 * @author Vesa Lappalainen, Mika Koskela
 * @version 1.0, 22.02.2003
 * @version 1.1, 8.3.2014
 *
 */
public class SailoException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Poikkeuksen muodostaja jolle vied��n poikkeuksen viesti
	 * @param viesti ilmoitettava viesti
	 */
	public SailoException(String viesti){
		super(viesti);
	}

}
