/**
 * 
 */
package Lainalista;

import java.util.Collection;
import java.util.List;

/**
 * - Huolehtii kaikkien avustajina olevien luokkien   
 *   v�lisest� yhteisty�st�                           
 * - lukee ja kirjoittaa lainalistan tiedostoon pyyt�-
 *   m�ll� apua avustajiltaan
 * 
 * @author Mika Koskela, Ville-Veikko V�h�aho
 * @version 1.0, 26.3.2014
 *
 */
public class LainaLista {

    private final Omistukset omistukset = new Omistukset();
    private final Kortit kortit = new Kortit();
    private final Tyypit tyypit = new Tyypit();
    private final HenkiloRek henkilot = new HenkiloRek();
    private final Lainaukset lainat = new Lainaukset();

    /**
     * palautetaan henkil�iden (omistajien) lukum��r�
     * @return henkil�iden lukum��r�
     */
    public int getHenkiloita() {
	return henkilot.getLkm();
    }

    /**
     * @return korttien lukum��r�
     */
    public int getKortteja() {
	return kortit.getLkm();
    }

    /**
     * Palautetaan henkil�
     * @param i henkil�n tunnusnumero
     * @return haluttu henkil�
     */
    public Henkilo annaHenkilo(int i) {
	return henkilot.anna(i);
    }

    /**
     * Palautetaan kortti
     * @param i kortin tunnusnro
     * @return haluttu kortti
     */
    public Kortti annaKortti(int i) {
	return kortit.anna(i);
    }

    /**
     * Palautetaan kaikki henkil�n omistukset
     * @param henkilo henkil� -olio
     * @return lista omistuksista, miss� kyseinen henkil� on mukana
     */
    public List<Omistus> annaOmistus(Henkilo henkilo) {
	return omistukset.annaOmistukset(henkilo.getHloid());
    }

    /**
     * Palautetaan kaikki korttiin liittyv�t omistukset
     * @param kortti kortti mink� omistajat halutaan saada selville
     * @return lista omistuksista, miss� joku omistaa t�t� korttia
     */
    public List<Omistus> annaOmistus(Kortti kortti) {
	return omistukset.annaOmistuksetKortilla(kortti.getTunnusNro());
    }

    /**
     * Palautetaan kaikki kortit mit� henkil�ll� on lainassa
     * @param henkilo kortin nykyisen haltijan tunnusNro
     * @return lista l�ydetyist� lainauksista
     */
    public List<Lainaus> annaTuloLainaukset(Henkilo henkilo) {
	return lainat.annaLainaukset(henkilo.getHloid());
    }

    /**
     * Palautetaan kaikki kortit, mitk� henkil� on lainannut pois
     * @param henkilo henkil�, joka omistaa kortin
     * @return lista l�ydetyist� lainauksista
     */
    public List<Lainaus> annaPoisLainatut(Henkilo henkilo) {
	return lainat.annaPoisLainatut(henkilo.getHloid());
    }

    /**
     * Lis�t��n kortti listalle
     * @param kortti lis�tt�v� kortti
     * @throws SailoException jos lis�ys ei onnistu
     */
    public void lisaa(Kortti kortti) throws SailoException {
	kortit.lisaa(kortti);
    }

    /**
     * Lis�t��n henkil� listalle
     * @param hlo lis�tt�v� henkil�
     * @throws SailoException ilmoittaa jos lis�ys ei onnistu
     */
    public void lisaa(Henkilo hlo) throws SailoException {
	henkilot.lisaa(hlo);
    }

    /**
     * Lis�t��n listalle uusi omistussuhde
     * @param omistus omistusmerkint� mik� lis�t��n
     */
    public void lisaa(Omistus omistus) {
	omistukset.lisaa(omistus);
    }

    /**
     * Lis�t��n tyyppi Lainalistalle
     * @param tyyppi lis�tt�v� tyyppi (esim creature)
     * @throws SailoException virheilmoitus, jos lis��minen ei onnistu
     */
    public void lisaa(Tyyppi tyyppi) throws SailoException {
	tyypit.lisaa(tyyppi);
    }

    /**
     * Lis�t��n uusi lainaus
     * @param laina lis�tt�v� lainaus
     */
    public void lisaa(Lainaus laina) {
	lainat.lisaa(laina);
    }

    /**
     * Luetaan listan tiedot tiedostosta
     * @param nimi tiedoston nimi
     * @throws SailoException virhe, jos lukeminen ei onnistu
     */
    public void lueTiedosto(String nimi) throws SailoException {
	tyypit.lueTiedostosta(nimi);
	henkilot.lueTiedostosta(nimi);
	kortit.lueTiedosto(nimi);
	omistukset.lueTiedosto(nimi);
	lainat.lueTiedosto(nimi);
    }

    /**
     * Tallentaa muutokset tiedostoon
     * @throws SailoException virhe jos ei onnistu
     */
    public void talleta() throws SailoException {
	tyypit.tallenna();
	henkilot.tallenna();
	kortit.talleta();
	omistukset.talleta();
	lainat.talleta();
    }

    /**
     * Ohjelma luokan testausta varten
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	LainaLista lista = new LainaLista();

	try {
	    Kortti eka = new Kortti(), toka = new Kortti();
	    eka.rekisteroi();
	    eka.vastaaSnap();
	    toka.rekisteroi();
	    toka.vastaaSnap();
	    lista.lisaa(eka);
	    lista.lisaa(toka);

	    Henkilo hlo1 = new Henkilo(), hlo2 = new Henkilo();
	    hlo1.rekisteroi();
	    hlo1.vastaaHenkilo();
	    hlo2.rekisteroi();
	    hlo2.vastaaHenkilo2();
	    lista.lisaa(hlo1);
	    lista.lisaa(hlo2);

	    Omistus snap1 = new Omistus(), snap2 = new Omistus();
	    snap1.rekisteroi();
	    snap1.vastaaAnttiCavern(hlo1.getHloid());
	    snap2.rekisteroi();
	    snap2.vastaaAnttiCavern(hlo2.getHloid());
	    lista.lisaa(snap1);
	    lista.lisaa(snap2);

	    Tyyppi instant = new Tyyppi(), creature = new Tyyppi();
	    instant.rekisteroi();
	    instant.vastaaTyyppi();
	    creature.rekisteroi();
	    creature.vastaaTyyppi2();
	    lista.lisaa(instant);
	    lista.lisaa(creature);

	    Lainaus laina1 = new Lainaus(), laina2 = new Lainaus();
	    laina1.rekisteroi();
	    laina1.vastaaApuLainaus(1);
	    laina2.rekisteroi();
	    laina2.vastaaApuLainaus(1);
	    lista.lisaa(laina1);
	    lista.lisaa(laina2);

	    System.out.println("=====Testi======");

	    for (int i = 0; i < lista.getHenkiloita(); i++) {
		Henkilo hlo = lista.annaHenkilo(i);
		System.out.print("Henkil�: ");
		hlo.tulosta(System.out);

		List<Omistus> loytyneet = lista.annaOmistus(hlo);
		if (loytyneet.isEmpty())
		    System.out.println("Ei omista viel� yht��n korttia");
		for (Omistus omistus : loytyneet) {
		    omistus.tulosta(System.out);
		}

		List<Lainaus> loytyneetLainat = lista.annaTuloLainaukset(hlo);
		if (loytyneetLainat.isEmpty())
		    System.out.println("Ei ole lainannut yht��n korttia");
		for (Lainaus lainaus : loytyneetLainat) {
		    lainaus.tulosta(System.out);
		}
	    }

	} catch (SailoException ex) {
	    System.out.println(ex.getMessage());
	}
    }

    /**
     * Poistetaan henkil�
     * @param id poistettavan henkil�n tunnusnumero
     * @return 0 jos ei onnistunut, 1 jos onnistui
     */
    public int poista(int id) {
	int ret = henkilot.poista(id);

	return ret;
    }

    /**
     * @param haku merkkijono mill� haetaan henkil�it�
     * @param numero haettavan henkil�n tunnusnumero
     * @return lista l�ydetyist� henkil�ist�
     */
    public List<Henkilo> etsi(String haku, int numero) {
	return henkilot.etsi(haku, numero);
    }

    /**
     * @param haku merkkijono mill� haetaan kortteja
     * @param numero haettavan kortin tunnusnumero
     * @return lista l�ydetyist� korteista
     */
    public Collection<Kortti> etsiKortti(String haku, int numero) {
	return kortit.etsi(haku, numero);
    }

    /** 
     * Palautetaan tyyppi 
     * @param i palautettavan tyypin indeksi 
     * @return haluttu tyyppi 
     */
    public Tyyppi annaTyyppi(int i) {
	return tyypit.anna(i - 1);
    }

    /**
     * Tarkastetaan, onko kyseisell� henkil�ll� ja kyseist� korttia hallussaan
     * @param kasiteltavaHenkilo henkil�olio
     * @param kasiteltavaKortti korttiolio
     * @return null, jos ei l�ydy, muuten olemassaoleva omistus
     */
    public Omistus tarkastaOmistukset(Henkilo kasiteltavaHenkilo, Kortti kasiteltavaKortti) {
	return omistukset.tarkastaOmistukset(kasiteltavaHenkilo.getHloid(),
		kasiteltavaKortti.getTunnusNro());

    }

    /**
     * Selvitet��n, ett� onko omistukseen liittyv�lt� henkil�lt� lainattu kyseist� korttia pois
     * @param omistus omistus, mink� perusteella katsotaan montako kyseist� korttia omistajalla on hallussaan
     * @return omistajalla hallussa olevien korttien m��r�
     */
    public int laskeNykyisetKortit(Omistus omistus) {
	return omistus.getMaara() - lainat.annaHalututLainat(omistus.getHloId(), omistus.getId());
    }

    /**
     * @return lista, miss� on kaikki henkil�t
     */
    public Henkilo[] annaHenkilot() {
	return henkilot.annaKaikki();
    }

    /**
     * Poistetaan kaikki annetut omistukset
     * @param poistettava lista poistettavista omistuksista
     * @return lista lopuista omistuksista
     */
    public Object poistaOmistukset(List<Omistus> poistettava) {
	return omistukset.poista(poistettava);
    }

    /**
     * @param poistettava omistus jota poistetaan
     */
    public void poistaOmistus(Omistus poistettava) {
	omistukset.poista(poistettava);
    }

    /**
     * Poistetaan lainaus, eli palautetaan kortti omistajalle
     * @param poistettava poistettava lainaus
     */
    public void palautaKortti(Lainaus poistettava) {
	lainat.poista(poistettava);
    }

    /**
     * annetaan listana kaikki lainaukset, jotka henkil� on antanut muille
     * @param kasiteltavaHenkilo omistaja
     * @return lista lainauksista, miss� omistaja on mukana
     */
    public List<Lainaus> annaMenoLainaukset(Henkilo kasiteltavaHenkilo) {
	return lainat.annaMenoLainaukset(kasiteltavaHenkilo);
    }

}
