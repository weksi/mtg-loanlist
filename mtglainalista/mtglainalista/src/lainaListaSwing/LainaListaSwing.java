/**
 * 
 */
package lainaListaSwing;

import java.awt.Desktop;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import Lainalista.Henkilo;
import Lainalista.Kortti;
import Lainalista.LainaLista;
import Lainalista.Lainaus;
import Lainalista.Omistus;
import Lainalista.SailoException;
import Naytto.LisaaKorttiOmistajalle;
import Naytto.Paaikkuna;
import Naytto.ValitseHlo;
import fi.jyu.mit.gui.AbstractChooser;
import fi.jyu.mit.gui.ComboBoxChooser;
import fi.jyu.mit.gui.IStringListChooser;
import fi.jyu.mit.gui.SelectionChangeListener;
import fi.jyu.mit.gui.StringTable;

/**
 * @author Mika Koskela, Ville-Veikko V�h�aho
 * @version 1.0, 5.4.2014
 * 
 */
public class LainaListaSwing {

    private final LainaLista lainalista;
    private AbstractChooser<Kortti> listKortit;
    private JTextArea textAreaKortit;
    private AbstractChooser<Henkilo> listHenkilot;
    private JPanel panelOmistajaPaa;
    private JTextArea areaOmistaja;
    private JTextField textField;
    private StringTable taulukkoCards;
    private JLabel labelCardHeading;
    private StringTable stringTableOwnedCards;
    private JLabel labelOwnerheading;
    private StringTable stringTableOwnBorrowedTo;
    private JTextField textSearch;
    private Paaikkuna paaGUI;
    private JTextPane textPaneCurrentCards;
    private ComboBoxChooser cBChooserNimet;
    private ComboBoxChooser cBChooserLkm;
    private StringTable stringTableBorFroOthers;

    private Kortti kasiteltavaKortti;
    private Henkilo kasiteltavaHenkilo;
    private int muokattu = 0;

    /**
     * Oletusmuodostaja
     */
    public LainaListaSwing() {
	lainalista = new LainaLista();
    }

    /**
     * Menn��n kurssin wiki-sivulle katsomaan ohjelman suunnitelmaa
     */
    public void apu() {
	Desktop kone = Desktop.getDesktop();
	URI uri;
	try {
	    uri = new URI(
		    "https://trac.cc.jyu.fi/projects/ohj2ht/wiki/k2014/suunnitelmat/mtglainalista");
	    kone.browse(uri);
	} catch (URISyntaxException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

    /**
     * alustetaan k�ytett�v�t alueet k�ytett�v�ksi, lis�t��n kuuntelijoita
     */
    public void alusta() {

	listKortit.addSelectionChangeListener(new SelectionChangeListener<Kortti>() {
	    @Override
	    public void selectionChange(IStringListChooser<Kortti> sender) {
		naytaKortti();
	    }
	});

	listHenkilot.addSelectionChangeListener(new SelectionChangeListener<Henkilo>() {
	    @Override
	    public void selectionChange(IStringListChooser<Henkilo> sender) {
		naytaHenkilo();
		;
	    }
	});

	getTextField().addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyReleased(KeyEvent e) {
		haeHlo(-1);
	    }
	});

	getTextSearch().addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyReleased(KeyEvent e) {
		haeKortti(-1);
	    }
	});

	luoNaytto();
	haeKortti(0);
	haeHlo(0);
    }

    /**
     * Alustetaan omistuksien lis�ysikkuna k�ytt��n
     */
    public void alustaLisays() {
	getTextSearch().addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyReleased(KeyEvent e) {
		haeKortti(-1);
	    }
	});

	listKortit.addSelectionChangeListener(new SelectionChangeListener<Kortti>() {
	    @Override
	    public void selectionChange(IStringListChooser<Kortti> sender) {
		naytaKortti();
	    }
	});

	haeKortti(0);
    }

    /**
     * N�ytet��n listalla valitun henkil�n tiedot
     */
    protected void naytaHenkilo() {
	int index = listHenkilot.getSelectedIndex();
	if (index < 0)
	    return;
	kasiteltavaHenkilo = listHenkilot.getSelectedObject();

	naytaHenkilot();
	naytaLainaukset();
	labelOwnerheading.setText(kasiteltavaHenkilo.getNimi());
    }

    private void naytaHenkilot() {
	if (kasiteltavaHenkilo == null)
	    return;
	stringTableOwnedCards.clear();
	stringTableOwnedCards.getTable().getColumnModel().getColumn(0).setMinWidth(50);
	List<Omistus> omistukset = lainalista.annaOmistus(kasiteltavaHenkilo);
	for (Omistus omistus : omistukset) {
	    int r = stringTableOwnedCards.addRow();
	    stringTableOwnedCards.setObjectAt(omistus, r);
	    stringTableOwnedCards.setValueAtModel(lainalista.annaKortti(omistus.anna(2) - 1)
		    .getNimi(), r, 0);
	    stringTableOwnedCards.setValueAtModel(omistus.anna(3), r, 1);
	    stringTableOwnedCards.setValueAtModel(lainalista.laskeNykyisetKortit(omistus), r, 2);
	}
    }

    private void luoNaytto() {
	/*
	 * getPanelOmistajaPaa().removeAll(); getPanelOmistajaPaa().add(getAreaOmistaja());
	 */
    }

    /**
     * N�ytet��n listassa valitun kortin tiedot omalla alueellaan
     */
    protected void naytaKortti() {
	int index = listKortit.getSelectedIndex();
	if (index < 0)
	    return;
	kasiteltavaKortti = listKortit.getSelectedObject();
	naytaOmistukset();
	labelCardHeading.setText(kasiteltavaKortti.getNimi() + " - "
		+ lainalista.annaTyyppi(kasiteltavaKortti.getTyyppi()).getTyyppi());
    }

    /**
     * N�ytet��n listassa hakuehdon mukaiset kortit
     * @param idNro kortin tunnusNumero
     */
    public void haeKortti(int idNro) {
	int kn = idNro;
	if (kn < 0 && kasiteltavaKortti != null)
	    kn = kasiteltavaKortti.getTunnusNro();

	String haku = getTextSearch().getText();
	if (haku.indexOf('*') < 0)
	/* ; */
	{
	    haku = "*" + haku + "*";
	}
	int numero = lainalista.getKortteja();
	Collection<Kortti> haunTulos = lainalista.etsiKortti(haku, numero);

	listKortit.clear();
	int index = 0;
	int i = 0;
	for (Kortti kortti : haunTulos) {
	    if (kortti.getTunnusNro() == idNro)
		index = i;
	    listKortit.add(kortti.getNimi(), kortti);
	    i++;
	}
	listKortit.setSelectedIndex(index);
    }

    private void naytaOmistukset() {
	if (kasiteltavaKortti == null)
	    return;
	taulukkoCards.clear();
	taulukkoCards.getTable().getColumnModel().getColumn(0).setMinWidth(50);

	List<Omistus> omistukset = lainalista.annaOmistus(kasiteltavaKortti);
	int r;
	for (Omistus omistus : omistukset) {
	    r = taulukkoCards.addRow();
	    taulukkoCards.setObjectAt(omistus, r);
	    taulukkoCards.setValueAtModel(lainalista.annaHenkilo(omistus.anna(1)).getNimi(), r,
		    0);
	    taulukkoCards.setValueAtModel(omistus.anna(3), r, 1);
	    taulukkoCards.setValueAtModel(lainalista.laskeNykyisetKortit(omistus), r, 2);
	}
    }

    /**
     * @param out tietovirta minne tulostetaan
     * @param kortti kortti jonka tiedot tulostetaan
     */
    public void tulosta(PrintStream out, final Kortti kortti) {
	out.println("===============");
	kortti.tulosta(out);
	out.println("===============");
	List<Omistus> omistukset = lainalista.annaOmistus(kortti);
	for (Omistus omistus : omistukset) {
	    omistus.tulosta(out);
	}
    }

    /**
     * Tulostaa yleisi� tietoja ohjelmasta
     */
    public void tiedot() {
	JOptionPane.showMessageDialog(null,
		"Ohjelma mtg-korttien lainojen hallinnointiin. versio 1.0, tekij�t: Mika ja Weksi",
		"Tietoja", 1);
    }

    /**
     * Poistetaan listasta valittu j�sen ja kaikki j�senen omistukset
     */
    public void poistaHlo() {
	if (kasiteltavaHenkilo == null)
	    return;
	int vastaus = JOptionPane.showConfirmDialog(null, "Poistetaanko j�sen: "
		+ kasiteltavaHenkilo.getNimi(), "Poisto?", JOptionPane.YES_NO_OPTION);
	if (vastaus == JOptionPane.NO_OPTION)
	    return;

	List<Lainaus> lainaukset = lainalista.annaTuloLainaukset(kasiteltavaHenkilo);
	for (Lainaus lainaus : lainaukset)
	    lainalista.palautaKortti(lainaus);
	lainaukset = lainalista.annaMenoLainaukset(kasiteltavaHenkilo);
	for (Lainaus lainaus2 : lainaukset)
	    lainalista.palautaKortti(lainaus2);
	
	List<Omistus> poistettava = lainalista.annaOmistus(kasiteltavaHenkilo);
	lainalista.poistaOmistukset(poistettava);
	lainalista.poista(kasiteltavaHenkilo.getHloid());

	int index = listHenkilot.getSelectedIndex();
	haeHlo(0);
	muokattu++;
	listHenkilot.setSelectedIndex(index);
    }

    /**
     * Poistetaan yksitt�inen omistus
     */
    public void poistaOmistus() {
	if (stringTableOwnedCards.getSelectedRowAtModel() < 0)
	    return;
	int vastaus = JOptionPane.showConfirmDialog(paaGUI, "Remove the card", "Poisto?",
		JOptionPane.YES_NO_OPTION);
	if (vastaus == JOptionPane.NO_OPTION)
	    return;

	Omistus poistettava = (Omistus) stringTableOwnedCards.getSelectedObject();
	lainalista.poistaOmistus(poistettava);

	naytaHenkilo();
	muokattu++;
    }

    /**
     * Palautetaan yksitt�ist� korttia kaikki kpl:t omistajalle
     */
    public void palautaKortti() {
	if (stringTableBorFroOthers.getSelectedRowAtModel() < 0)
	    return;

	Lainaus poistettava = (Lainaus) stringTableBorFroOthers.getSelectedObject();
	lainalista.palautaKortti(poistettava);

	naytaLainaukset();
	muokattu++;
    }

    /**
     * tallentaa tiedoston jos tarvetta sille
     */
    public void tallenna() {
	if (muokattu == 0)
	    return;
	try {
	    lainalista.talleta();
	} catch (SailoException e) {
	    JOptionPane.showMessageDialog(null, e.getMessage());
	}
	muokattu = 0;
    }

    /**
     * Luodaan uusi henkil�
     */
    public void luoHenkilo() {
	String nimi = JOptionPane.showInputDialog(paaGUI, "Give the name of the new Owner");
	if (nimi != null && !nimi.equals("")) {
	    kasiteltavaHenkilo = new Henkilo(nimi);
	    kasiteltavaHenkilo.rekisteroi();

	    try {
		lainalista.lisaa(kasiteltavaHenkilo);
		muokattu++;
		haeHlo(kasiteltavaHenkilo.getHloid());
	    } catch (SailoException e) {
		JOptionPane.showMessageDialog(null, e.getMessage());
	    }
	} else
	    JOptionPane.showMessageDialog(paaGUI, "Couldn't add a new owner");
	return;
    }

    protected void haeHlo(int hloId) {
	int hn = hloId;
	if (hn < 0 && kasiteltavaHenkilo != null)
	    hn = kasiteltavaHenkilo.getHloid();

	String haku = getTextField().getText();
	if (haku.indexOf('*') < 0) {
	    haku = "*" + haku + "*";
	}
	int numero = lainalista.getHenkiloita();
	Collection<Henkilo> haunTulos = lainalista.etsi(haku, numero);

	listHenkilot.clear();
	int index = 0;
	int i = 0;
	for (Henkilo henkilo : haunTulos) {
	    if (henkilo.getHloid() == hloId)
		index = i;
	    listHenkilot.add(henkilo.getNimi(), henkilo);
	    i++;
	}
	listHenkilot.setSelectedIndex(index);

	naytaHenkilot();
    }

    /**
     * Laitetaan lainalista -luokalle k�sky lukea tiedosto
     */
    public void lueTiedosto() {
	try {
	    lainalista.lueTiedosto(null);
	} catch (SailoException e) {
	    JOptionPane.showMessageDialog(null, e.getMessage());
	}
	alusta();
	haeKortti(0);
	haeHlo(0);
    }

    /**
     * lis�t��n valitulle henkil�lle kortteja (omistuksia) 
     */
    public void lisaaOmistus() {
	new LisaaKorttiOmistajalle(this).setVisible(true);
	haeHlo(listHenkilot.getSelectedObject().getHloid());
	naytaOmistukset();
	paaGUI.otaKomponentitHaltuun();
    }

    /**
     * K�yt�nn�ss� luodaan uusi omistus-olio saadulla m��r�ll�
     * k�sitelt�v�lle kortille ja henkil�lle
     * @param maara lis�tt�vien korttien m��r�
     */
    public void rekisteroiOmistus(int maara) {
	Omistus lisattava = tarkastaOmistukset();
	if (lisattava != null) {
	    lisattava.lisaaKortteja(maara);
	    muokattu++;
	    return;
	}
	lisattava = new Omistus(kasiteltavaHenkilo.getHloid(), kasiteltavaKortti.getTunnusNro(),
		maara);
	lisattava.rekisteroi();
	lainalista.lisaa(lisattava);

	muokattu++;
    }

    private Omistus tarkastaOmistukset() {
	return lainalista.tarkastaOmistukset(kasiteltavaHenkilo, kasiteltavaKortti);
    }

    /**
     * Lis�t��n lainaus
     */
    public void lisaaLainaus() {
	Lainaus lisattava = new Lainaus();
	lisattava.rekisteroi();
	lisattava.vastaaApuLainaus(kasiteltavaHenkilo.getHloid());
	lainalista.lisaa(lisattava);
	muokattu++;
	naytaLainaukset();
    }

    private void naytaLainaukset() {
	if (kasiteltavaHenkilo == null)
	    return;
	stringTableOwnBorrowedTo.clear();
	stringTableOwnBorrowedTo.getTable().getColumnModel().getColumn(0).setMinWidth(50);

	List<Lainaus> lainaukset = lainalista.annaPoisLainatut(kasiteltavaHenkilo);
	for (Lainaus lainaus : lainaukset) {
	    int r = stringTableOwnBorrowedTo.addRow();
	    stringTableOwnBorrowedTo.setObjectAt(lainaus, r);
	    stringTableOwnBorrowedTo.setValueAtModel(lainalista.annaKortti(lainaus.getKorttiId() - 1)
		    .getNimi(), r, 0);
	    stringTableOwnBorrowedTo.setValueAtModel(lainaus.getKpl(), r, 1);
	    stringTableOwnBorrowedTo.setValueAtModel(
		    lainalista.annaHenkilo(lainaus.getHaltijaId()).getNimi(), r, 2);
	}

	stringTableBorFroOthers.clear();
	stringTableBorFroOthers.getTable().getColumnModel().getColumn(0).setMinWidth(50);

	List<Lainaus> iteLainatut = lainalista.annaTuloLainaukset(kasiteltavaHenkilo);
	for (Lainaus lainaus : iteLainatut) {
	    int p = stringTableBorFroOthers.addRow();
	    stringTableBorFroOthers.setObjectAt(lainaus, p);
	    stringTableBorFroOthers.setValueAtModel(lainalista.annaKortti(lainaus.getKorttiId() - 1)
		    .getNimi(), p, 0);
	    stringTableBorFroOthers.setValueAtModel(lainaus.getKpl(), p, 1);
	    stringTableBorFroOthers.setValueAtModel(
		    lainalista.annaHenkilo(lainaus.getOmistajaId()).getNimi(), p, 2);
	}
    }

    /**
     * lainataan valittua korttia
     */
    public void lainaaKortti() {
	if (taulukkoCards.getSelectedObject() == null) {
	    JOptionPane.showMessageDialog(paaGUI, "Choose a player to loan cards from!");
	    return;
	}
	if (lainalista.annaOmistus(kasiteltavaKortti).isEmpty()) {
	    JOptionPane.showMessageDialog(paaGUI, "There are no cards to loan!");
	    return;
	}
	Omistus omistus = (Omistus) taulukkoCards.getSelectedObject();
	if (lainalista.laskeNykyisetKortit(omistus) <= 0) {
	    JOptionPane
		    .showMessageDialog(
			    paaGUI,
			    "This poor fellow doesn't have any of these cards left! \n Choosing someone else (if possible) might help.");
	    return;
	}

	new ValitseHlo(this).setVisible(true);
    }

    /**
     * luodaan lainaus annetuilla tiedoilla
     */
    public void rekisteroiLainaus() {
	Omistus omistus = (Omistus) taulukkoCards.getSelectedObject();
	try {
	    List<Henkilo> henkiloLista = lainalista.etsi(cBChooserNimet.getSelectedText(),
		    lainalista.getHenkiloita());

	    Lainaus lisattava = new Lainaus(henkiloLista.get(0).getHloid(), omistus.getId(),
		    Integer.parseInt(cBChooserLkm.getSelectedText()), omistus.getHloId());
	    lisattava.rekisteroi();
	    lainalista.lisaa(lisattava);
	    muokattu++;
	} catch (Exception e) {
	    JOptionPane.showMessageDialog(null,
		    "Whoops, something went wrong! \n " + e.getMessage());
	}
	alusta();
    }

    /**
     * alustetaan lainauksien tekemisikkuna
     */
    public void alustaLainaus() {
	Omistus omistus = (Omistus) taulukkoCards.getSelectedObject();
	Henkilo[] henkiloTaulukko = lainalista.annaHenkilot();
	for (Henkilo henkilo : henkiloTaulukko) {
	    if (henkilo != null && henkilo.getHloid() != omistus.getHloId())
		cBChooserNimet.add(henkilo.getNimi());
	}
	for (int i = 1; i <= lainalista.laskeNykyisetKortit(omistus); i++) {
	    cBChooserLkm.add("" + i);
	}

    }

    /**
     * kysyt��n ett� mit� henkil�� k�sitell��n
     */
    public void kysyNimi() {
	new ValitseHlo(this).setVisible(true);
	Henkilo[] henkiloTaulukko = lainalista.annaHenkilot();
	for (Henkilo henkilo : henkiloTaulukko) {
	    if (henkilo != null)
		cBChooserNimet.add(henkilo.getNimi());
	}
    }

    // / ========== T�st� alkaa getterit ja setterit ========== ///

    /**
     * @return taulukko miss� on korttien omistustietoja
     */
    public StringTable getTaulukkoCards() {
	return taulukkoCards;
    }

    /**
     * @param taulukkoCards taulukko mihin n�ytet��n kortin omistustietoja
     */
    public void setTaulukkoCards(StringTable taulukkoCards) {
	this.taulukkoCards = taulukkoCards;
    }

    /**
     * @return omistajien hakukentt�
     */
    public JTextField getTextField() {
	return textField;
    }

    /**
     * @param textField omistajien hakukentt�
     */
    public void setTextField(JTextField textField) {
	this.textField = textField;
    }

    /**
     * @return laatikko miss� on valitun kortin nimi
     */
    public JLabel getLabelCardHeading() {
	return labelCardHeading;
    }

    /**
     * @param labelCardHeading teksti "laatikko" miss� on valitun kortin nimi
     */
    public void setLabelCardHeading(JLabel labelCardHeading) {
	this.labelCardHeading = labelCardHeading;
    }

    /**
     * @return Taulukko miss� on omistajan omistamat kortit
     */
    public StringTable getStringTableOwnedCards() {
	return stringTableOwnedCards;
    }

    /**
     * @param stringTableOwnedCards Taulukko miss� on omistajan omistamat kortit
     */
    public void setStringTableOwnedCards(StringTable stringTableOwnedCards) {
	this.stringTableOwnedCards = stringTableOwnedCards;
    }

    /**
     * @return Teksti miss� on valitun omistajan nimi
     */
    public JLabel getLabelOwnerheading() {
	return labelOwnerheading;
    }

    /**
     * @param labelOwnerheading Teksti miss� on valitun omistajan nimi
     */
    public void setLabelOwnerheading(JLabel labelOwnerheading) {
	this.labelOwnerheading = labelOwnerheading;
    }

    /**
     * @return taulukko miss� n�kyy muille lainatut kortit
     */
    public StringTable getStringTableOwnBorrowedTo() {
	return stringTableOwnBorrowedTo;
    }

    /**
     * @param stringTableOwnBorrowedTo taulukko miss� n�kyy muille lainatut kortit
     */
    public void setStringTableOwnBorrowedTo(StringTable stringTableOwnBorrowedTo) {
	this.stringTableOwnBorrowedTo = stringTableOwnBorrowedTo;
    }

    /**
     * @return korttien hakukentt�
     */
    public JTextField getTextSearch() {
	return textSearch;
    }

    /**
     * @param textSearch korttien hakukentt�
     */
    public void setTextSearch(JTextField textSearch) {
	this.textSearch = textSearch;
    }

    /**
     * @return k�ytett�v�n ohjelman p��ikkuna
     */
    public Paaikkuna getPaaGUI() {
	return paaGUI;
    }

    /**
     * @param paaGUI k�ytett�v�n ohjelman p��ikkuna
     */
    public void setPaaGUI(Paaikkuna paaGUI) {
	this.paaGUI = paaGUI;
    }

    /**
     * @return teksti mik� n�kyy lainausikkunassa
     */
    public JTextPane getTextPaneCurrentCards() {
	return textPaneCurrentCards;
    }

    /**
     * @param textPaneCurrentCards teksti mik� n�kyy lainausikkunassa
     */
    public void setTextPaneCurrentCards(JTextPane textPaneCurrentCards) {
	this.textPaneCurrentCards = textPaneCurrentCards;
    }

    /**
     * @return alasvetovalikko miss� on nimet
     */
    public ComboBoxChooser getComboBoxChooser() {
	return cBChooserNimet;
    }

    /**
     * @param comboBoxChooser alasvetovalikko miss� on nimet
     */
    public void setComboBoxChooser(ComboBoxChooser comboBoxChooser) {
	this.cBChooserNimet = comboBoxChooser;
    }

    /**
     * @return alasvetovalikko, miss� on korttien lukum��r�t valittuna
     */
    public ComboBoxChooser getcBChooserLkm() {
	return cBChooserLkm;
    }

    /**
     * @param cBChooserLkm alasvetovalikko, miss� on korttien lukum��r�t valittuna
     */
    public void setcBChooserLkm(ComboBoxChooser cBChooserLkm) {
	this.cBChooserLkm = cBChooserLkm;
    }

    /**
     * @return taulukko miss� n�kyy muilta lainatut kortit
     */
    public StringTable getStringTableBorFroOthers() {
	return stringTableBorFroOthers;
    }

    /**
     * @param stringTableBorFroOthers taulukko miss� n�kyy muilta lainatut kortit
     */
    public void setStringTableBorFroOthers(StringTable stringTableBorFroOthers) {
	this.stringTableBorFroOthers = stringTableBorFroOthers;
    }

    /**
     * @return alue jolla n�kyy kaikki omistajaan liittyv�t tiedot
     */
    public JPanel getPanelOmistajaPaa() {
	return panelOmistajaPaa;
    }

    /**
     * @param panelOmistajaPaa alue mihin tulostetaan kaikki omistajan tiedot
     */
    public void setPanelOmistajaPaa(JPanel panelOmistajaPaa) {
	this.panelOmistajaPaa = panelOmistajaPaa;
    }

    /**
     * @return alue johon testausta varten tulostetaan omistajan tiedot
     */
    public JTextArea getAreaOmistaja() {
	return areaOmistaja;
    }

    /**
     * @param areaOmistaja alue johon testausta varten tulostetaan omistajan
     * tiedot
     */
    public void setAreaOmistaja(JTextArea areaOmistaja) {
	this.areaOmistaja = areaOmistaja;
    }

    /**
     * @return lista miss� n�kyy kaikkien omistajien nimet
     */
    public AbstractChooser<Henkilo> getListHenkilot() {
	return listHenkilot;
    }

    /**
     * @param listHenkilot lista miss� n�kyy kaikkien omistajien nimet
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void setListHenkilot(AbstractChooser listHenkilot) {
	this.listHenkilot = listHenkilot;
    }

    /**
     * @return lista jossa n�kyy kaikki olevat kortit
     */
    public AbstractChooser<Kortti> getListKortit() {
	return listKortit;
    }

    /**
     * @param listKortit lista mihin tulee korttien nimet (mutta ei muuta)
     * n�kyviin
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void setListKortit(AbstractChooser listKortit) {
	this.listKortit = listKortit;
    }

    /**
     * @return alue johon testausvaiheessa tulostetaan kortin tiedot
     */
    public JTextArea getTextAreaKortit() {
	return textAreaKortit;
    }

    /**
     * @param textAreaKortit alue johon testausvaiheessa kirjoitetaan korttien
     * tiedot
     */
    public void setTextAreaKortit(JTextArea textAreaKortit) {
	this.textAreaKortit = textAreaKortit;
    }

}
