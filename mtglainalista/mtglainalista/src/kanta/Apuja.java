/**
 * 
 */
package kanta;


/**
 * @author Mika
 * @version 1.0, 8.3.2014
 * 
 */
public class Apuja {

	/**
	 * luodaan satunnaisluku halutusta lukuv�list�
	 * @param min lukuv�lin alaraja
	 * @param max lukuv�lin yl�raja
	 * @return satunnaisluku kokonaisluvuksi muutettuna
	 */
	public static double rand(double min, double max) {
		double n = (max - min) * Math.random() + min;
		return (int) Math.round(n);
	}

	/**
	 * @param args ei k�yt�ss�
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
