package Naytto;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

/**
 * @author Mika Koskela
 * @version 1.0, 24.2.2014
 *
 */
public class Owner extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanelPohja = new JPanel();
    private JTextField textField;

    /**
     * Launch the application.
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	try {
	    Owner dialog = new Owner();
	    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    dialog.setVisible(true);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Create the dialog.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Owner() {
    	setModalityType(ModalityType.APPLICATION_MODAL);
	setTitle("Owner");
	setBounds(100, 100, 450, 300);
	getContentPane().setLayout(new BorderLayout());
	contentPanelPohja.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanelPohja, BorderLayout.CENTER);
	contentPanelPohja.setLayout(new BorderLayout(0, 0));
	{
	    JSplitPane splitPaneTausta = new JSplitPane();
	    contentPanelPohja.add(splitPaneTausta);
	    {
		JPanel panelHakemisto = new JPanel();
		splitPaneTausta.setLeftComponent(panelHakemisto);
		panelHakemisto.setLayout(new BorderLayout(0, 0));
		{
		    textField = new JTextField();
		    panelHakemisto.add(textField, BorderLayout.NORTH);
		    textField.setColumns(10);
		}
		{
		    JList list = new JList();
		    list.setModel(new AbstractListModel() {
			/**
			* 
			*/
			private static final long serialVersionUID = 1L;
			String[] values = new String[] { "Weksi", "Mika", "Antti", "Juho" };

			@Override
			public int getSize() {
			    return values.length;
			}

			@Override
			public Object getElementAt(int index) {
			    return values[index];
			}
		    });
		    panelHakemisto.add(list);
		}
	    }
	    {
		JPanel panel = new JPanel();
		splitPaneTausta.setRightComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));
		{
		    nimiPaneeli nimiPaneeliKeski = new nimiPaneeli();
		    nimiPaneeliKeski.setLblNameText("Owner");
		    panel.add(nimiPaneeliKeski);
		}
	    }
	}
	{
	    JPanel buttonPane = new JPanel();
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    buttonPane.setLayout(new BorderLayout(0, 0));
	    {
		JPanel panelAddDel = new JPanel();
		buttonPane.add(panelAddDel, BorderLayout.WEST);
		FlowLayout fl_panelAddDel = (FlowLayout) panelAddDel.getLayout();
		fl_panelAddDel.setAlignment(FlowLayout.LEFT);
		{
		    JButton btnAddOwner = new JButton("Add Owner");
		    btnAddOwner.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			    //LainaListaSwing.luoHenkilo();
			}
		    });
		    panelAddDel.add(btnAddOwner);
		}
		{
		    JButton btnDelete = new JButton("Delete");
		    btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			    //Paaikkuna.poistaHenkilo();
			}
		    });
		    panelAddDel.add(btnDelete);
		}
	    }
	    {
		JPanel panelButtons = new JPanel();
		FlowLayout fl_panelButtons = (FlowLayout) panelButtons.getLayout();
		fl_panelButtons.setAlignment(FlowLayout.RIGHT);
		buttonPane.add(panelButtons, BorderLayout.EAST);
		{
		    JButton okButton = new JButton("OK");
		    okButton.addActionListener(new ActionListener() {
		    	@Override
                public void actionPerformed(ActionEvent arg0) {
		    		//Paaikkuna.hyvaksy();
		    	}
		    });
		    panelButtons.add(okButton);
		    okButton.setAlignmentX(Component.RIGHT_ALIGNMENT);
		    okButton.setActionCommand("OK");
		    getRootPane().setDefaultButton(okButton);
		}
		{
		    JButton cancelButton = new JButton("Cancel");
		    cancelButton.addActionListener(new ActionListener() {
		    	@Override
				public void actionPerformed(ActionEvent arg0) {
		    		//Paaikkuna.peruuta();
		    	}
		    });
		    panelButtons.add(cancelButton);
		    cancelButton.setAlignmentX(Component.RIGHT_ALIGNMENT);
		    cancelButton.setActionCommand("Cancel");
		}
	    }
	}
    }
}
