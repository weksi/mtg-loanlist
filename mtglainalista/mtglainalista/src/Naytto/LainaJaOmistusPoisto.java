package Naytto;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * @author Mika Koskela ja Ville-Veikko V�h�aho
 * @version 1.0, 29.4.2014
 *
 */
public class LainaJaOmistusPoisto extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JList list;
    private JList list_1;
    private JButton btnNewButton;
    private JButton btnNewButton_1;

    /**
     * Launch the application.
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		try {
		    LainaJaOmistusPoisto frame = new LainaJaOmistusPoisto();
		    frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the frame.
     */
    public LainaJaOmistusPoisto() {
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(100, 100, 450, 300);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	GridBagLayout gbl_contentPane = new GridBagLayout();
	gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0 };
	gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
		1.0, Double.MIN_VALUE };
	gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
	contentPane.setLayout(gbl_contentPane);

	JLabel lblNewLabel = new JLabel("Loans");
	GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
	gbc_lblNewLabel.gridwidth = 5;
	gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
	gbc_lblNewLabel.gridx = 1;
	gbc_lblNewLabel.gridy = 0;
	contentPane.add(lblNewLabel, gbc_lblNewLabel);

	JLabel lblNewLabel_1 = new JLabel("Ownerships");
	GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
	gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
	gbc_lblNewLabel_1.gridx = 7;
	gbc_lblNewLabel_1.gridy = 0;
	contentPane.add(lblNewLabel_1, gbc_lblNewLabel_1);

	textField = new JTextField();
	GridBagConstraints gbc_textField = new GridBagConstraints();
	gbc_textField.gridwidth = 5;
	gbc_textField.insets = new Insets(0, 0, 5, 5);
	gbc_textField.fill = GridBagConstraints.HORIZONTAL;
	gbc_textField.gridx = 1;
	gbc_textField.gridy = 1;
	contentPane.add(textField, gbc_textField);
	textField.setColumns(10);

	textField_1 = new JTextField();
	GridBagConstraints gbc_textField_1 = new GridBagConstraints();
	gbc_textField_1.insets = new Insets(0, 0, 5, 5);
	gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
	gbc_textField_1.gridx = 7;
	gbc_textField_1.gridy = 1;
	contentPane.add(textField_1, gbc_textField_1);
	textField_1.setColumns(10);

	list = new JList();
	GridBagConstraints gbc_list = new GridBagConstraints();
	gbc_list.gridwidth = 5;
	gbc_list.insets = new Insets(0, 0, 5, 5);
	gbc_list.fill = GridBagConstraints.BOTH;
	gbc_list.gridx = 1;
	gbc_list.gridy = 2;
	contentPane.add(list, gbc_list);

	list_1 = new JList();
	GridBagConstraints gbc_list_1 = new GridBagConstraints();
	gbc_list_1.insets = new Insets(0, 0, 5, 5);
	gbc_list_1.fill = GridBagConstraints.BOTH;
	gbc_list_1.gridx = 7;
	gbc_list_1.gridy = 2;
	contentPane.add(list_1, gbc_list_1);

	btnNewButton = new JButton("Remove loan");
	GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
	gbc_btnNewButton.gridwidth = 5;
	gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
	gbc_btnNewButton.gridx = 1;
	gbc_btnNewButton.gridy = 3;
	contentPane.add(btnNewButton, gbc_btnNewButton);

	btnNewButton_1 = new JButton("Remove an ownership");
	GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
	gbc_btnNewButton_1.insets = new Insets(0, 0, 0, 5);
	gbc_btnNewButton_1.gridx = 7;
	gbc_btnNewButton_1.gridy = 3;
	contentPane.add(btnNewButton_1, gbc_btnNewButton_1);
    }
}
