package Naytto;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import lainaListaSwing.LainaListaSwing;
import fi.jyu.mit.gui.ComboBoxChooser;

/**
 * Valitaan valikosta haluttu henkil�
 * @author Mika Koskela ja Ville-Veikko V�h�aho
 * @version 1.0, 28.4.2014
 *
 */
public class ValitseHlo extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    protected ComboBoxChooser comboBoxChooserHlo = new ComboBoxChooser();
    protected ComboBoxChooser comboBoxChooserLkm = new ComboBoxChooser();
    /**
     * @wbp.nonvisual location=65,349
     */
    private final LainaListaSwing lainaListaSwing;

    /**
     * Launch the application.
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	try {
	    ValitseHlo dialog = new ValitseHlo(null);
	    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    dialog.setVisible(true);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Create the dialog.
     * @param lainalistaswing swingi mit� k�ytet��n
     */
    public ValitseHlo(LainaListaSwing lainalistaswing) {
	setModalityType(ModalityType.APPLICATION_MODAL);
	this.lainaListaSwing = lainalistaswing;
	setBounds(100, 100, 450, 300);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(new BorderLayout(0, 0));
	{
	    JSplitPane splitPane = new JSplitPane();
	    splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
	    contentPanel.add(splitPane, BorderLayout.CENTER);
	    splitPane.setLeftComponent(comboBoxChooserHlo);
	    {

		comboBoxChooserHlo.getCaptionLabel().setHorizontalAlignment(SwingConstants.CENTER);
		comboBoxChooserHlo.getCaptionLabel().setFont(new Font("Tahoma", Font.BOLD, 16));
		comboBoxChooserHlo.getCaptionLabel().setText("Who are You?");
	    }
	    lainaListaSwing.setComboBoxChooser(comboBoxChooserHlo);
	    {

		comboBoxChooserLkm.getCaptionLabel().setHorizontalAlignment(SwingConstants.CENTER);
		comboBoxChooserLkm.getCaptionLabel().setText("How many do you wish to loan?");
		comboBoxChooserLkm.setFont(new Font("Tahoma", Font.BOLD, 15));
		splitPane.setRightComponent(comboBoxChooserLkm);
	    }
	    lainaListaSwing.setcBChooserLkm(comboBoxChooserLkm);
	}
	{
	    JPanel buttonPane = new JPanel();
	    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    {
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent arg0) {
			if (comboBoxChooserHlo.getSelectedText().length() <= 0) {
			    JOptionPane
				    .showMessageDialog(ValitseHlo.this, "No owners were chosen!");
			    return;
			}
			if (comboBoxChooserLkm.getSelectedText().length() <= 0) {
			    JOptionPane.showMessageDialog(ValitseHlo.this,
				    "You didn't choose any amount!");
			    return;
			}

			lainaListaSwing.rekisteroiLainaus();
			ValitseHlo.this.setVisible(false);
		    }
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	    }
	    {
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
	    }
	}
	lainaListaSwing.alustaLainaus();
    }

}
