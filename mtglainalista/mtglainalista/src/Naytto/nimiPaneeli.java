package Naytto;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/** 
 * @author Mika Koskela 
 * @version 1.0, 22.2.2014 
 * 
 */
public class nimiPaneeli extends JPanel {
    /** 
     *  
     */
    private static final long serialVersionUID = 1L;
    private final JLabel lblName = new JLabel("Name");
    private final JTextField tekstiAlue = new JTextField();

    /** 
     * Create the panel. 
     */
    public nimiPaneeli() {
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
	gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
	gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
	gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
	setLayout(gridBagLayout);

	GridBagConstraints gbc_lblName = new GridBagConstraints();
	gbc_lblName.insets = new Insets(0, 0, 0, 5);
	gbc_lblName.anchor = GridBagConstraints.EAST;
	gbc_lblName.gridx = 0;
	gbc_lblName.gridy = 1;
	add(lblName, gbc_lblName);
	tekstiAlue.setColumns(10);

	GridBagConstraints gbc_tekstiAlue = new GridBagConstraints();
	gbc_tekstiAlue.gridwidth = 2;
	gbc_tekstiAlue.insets = new Insets(0, 0, 0, 5);
	gbc_tekstiAlue.fill = GridBagConstraints.HORIZONTAL;
	gbc_tekstiAlue.gridx = 1;
	gbc_tekstiAlue.gridy = 1;
	add(tekstiAlue, gbc_tekstiAlue);

    }

    /**
     * @return tekstikentän sisältö
     */
    public String getLblNameText() {
	return lblName.getText();
    }

    /**
     * @param text uusi merkkijono
     */
    public void setLblNameText(String text) {
	lblName.setText(text);
    }
}
