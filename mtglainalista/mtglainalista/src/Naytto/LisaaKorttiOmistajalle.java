package Naytto;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import lainaListaSwing.LainaListaSwing;
import fi.jyu.mit.gui.ComboBoxChooser;
import fi.jyu.mit.gui.ListChooser;

/**
 * @author Mika Koskela ja Ville-Veikko V�h�aho
 * @version 1.0, 27.4.2014
 *
 */
public class LisaaKorttiOmistajalle extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextField textLisaysHaku;
    private ListChooser listChooserLisaysKortit = new ListChooser();
    private ComboBoxChooser cBChooserMaara = new ComboBoxChooser();
    private int MAX_KERTALISAYS = 20;
    /**
     * @wbp.nonvisual location=85,349
     */
    private final LainaListaSwing lainaListaSwing;

    /**
     * Launch the application.
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	try {
	    LisaaKorttiOmistajalle dialog = new LisaaKorttiOmistajalle(null);
	    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    dialog.setVisible(true);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Create the dialog.
     * @param lainalistaSwing swingi miss� tehd��n asiat
     */
    public LisaaKorttiOmistajalle(LainaListaSwing lainalistaSwing) {
	this.lainaListaSwing = lainalistaSwing;
	lainaListaSwing.setListKortit(listChooserLisaysKortit);
	setModalityType(ModalityType.APPLICATION_MODAL);
	setTitle("Add Cards to Owner");
	setBounds(100, 100, 450, 300);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(new BorderLayout(0, 0));
	{
	    JSplitPane splitPaneTausta = new JSplitPane();
	    contentPanel.add(splitPaneTausta, BorderLayout.CENTER);
	    {
		JSplitPane splitVasen = new JSplitPane();
		splitVasen.setName("splitLisays");
		splitVasen.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPaneTausta.setLeftComponent(splitVasen);
		{
		    textLisaysHaku = new JTextField();
		    textLisaysHaku.setName("LisaysHaku");
		    lainaListaSwing.setTextSearch(textLisaysHaku);
		    textLisaysHaku.setToolTipText("Search");
		    splitVasen.setLeftComponent(textLisaysHaku);
		    textLisaysHaku.setColumns(10);
		}
		{

		    listChooserLisaysKortit.setCaption("Choose the card to be added");
		    splitVasen.setRightComponent(listChooserLisaysKortit);
		}
	    }
	    {
		JPanel panelLisaysOikea = new JPanel();
		splitPaneTausta.setRightComponent(panelLisaysOikea);
		GridBagLayout gbl_panelLisaysOikea = new GridBagLayout();
		gbl_panelLisaysOikea.columnWidths = new int[] { 0, 0, 0 };
		gbl_panelLisaysOikea.rowHeights = new int[] { 30, 0, 30, 0 };
		gbl_panelLisaysOikea.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelLisaysOikea.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0 };
		panelLisaysOikea.setLayout(gbl_panelLisaysOikea);
		{
		    cBChooserMaara.getCaptionLabel().setText("Choose Amount");
		    GridBagConstraints gbc_cBChooserMaara = new GridBagConstraints();
		    gbc_cBChooserMaara.gridwidth = 2;
		    gbc_cBChooserMaara.insets = new Insets(0, 0, 5, 0);
		    gbc_cBChooserMaara.fill = GridBagConstraints.BOTH;
		    gbc_cBChooserMaara.gridx = 0;
		    gbc_cBChooserMaara.gridy = 1;
		    panelLisaysOikea.add(cBChooserMaara, gbc_cBChooserMaara);
		    for (int i = 1; i <= MAX_KERTALISAYS; i++)
			cBChooserMaara.add("" + i);
		}
	    }
	}
	{
	    JPanel buttonPane = new JPanel();
	    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    {
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent arg0) {
			if (getMaara() == -1) {
			    JOptionPane.showMessageDialog(LisaaKorttiOmistajalle.this, "Please choose the amount of cards to be added before accepting!");
			    return;
			}
			lainaListaSwing.rekisteroiOmistus(getMaara());
			LisaaKorttiOmistajalle.this.setVisible(false);
		    }
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	    }
	    {
		final JButton cancelButton = new JButton("Cancel");
		// getRootPane().setDefaultButton(cancelButton);
		cancelButton.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent arg0) {
			// processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			// processWindowEvent(new WindowEvent(contentPanel, WindowEvent.WINDOW_CLOSING));
			LisaaKorttiOmistajalle.this.setVisible(false);
		    }
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
	    }
	}
	lainaListaSwing.alustaLisays();
    }

    /* === Omaa koodia === */

    /**
     * Palautetaan alasvetovalikossa valittu m��r� kortteja
     * @return jos m��r�� ei ole valittu palautetaan -1, muuten valittu m��r�
     */
    protected int getMaara() {
	if (!cBChooserMaara.getKohde(cBChooserMaara.getSelectedIndex()).equals("")) return Integer.parseInt(cBChooserMaara.getKohde(cBChooserMaara.getSelectedIndex()));
	return -1;
    }

}
