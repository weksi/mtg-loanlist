/**
 * 
 */
package Lainalista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fi.jyu.mit.ohj2.WildChars;

/**
 * - osaa lis�t� ja poistaa kortin
 * - lukee ja kirjoittaa kortit tiedostoon
 * - osaa etsi� ja lajitella
 * 
 * Tehty Vesa Lappalaisen mallin pohjalta
 * @author Mika Koskela ja Ville-Veikko V�h�aho
 * @version 1.0, 8.3.2014
 * 
 */
public class Kortit {

    private static final int MAX_LKM = 20;
    private boolean muutettu = false;
    private int lkm = 0;
    private Kortti[] alkiot = new Kortti[MAX_LKM];

    private String tiedostoNimi = "";

    /**
     * Palautetaan korttien lukum��r�
     * @return korttien lukum��r�
     */
    public int getLkm() {
	return lkm;
    }

    /**
     * Lisataan taulukkoon uusi kortti
     * @param kortti lisattava kortti
     * @throws SailoException jos tietorakenne on jo t�ynn�
     * @example
     * <pre name="test">
     * #THROWS SailoException
     *   Kortti snap1 = new Kortti(), snap2 = new Kortti(); 
     *   Kortit kortit = new Kortit(); kortit.getLkm() === 0;
     *   kortit.lisaa(snap1); kortit.getLkm() === 1;
     *   kortit.lisaa(snap2); kortit.getLkm() === 2;
     *   kortit.lisaa(snap1); kortit.getLkm() === 3;
     *   kortit.lisaa(snap1); kortit.getLkm() === 4;
     *   kortit.anna(0) === snap1;
     *   kortit.anna(1) === snap2;
     *   kortit.anna(2) === snap1;
     *   kortit.anna(0) == snap1 === true;
     *   kortit.anna(0) == snap2 === false;
     *   kortit.anna(3) === snap1;
     *   kortit.anna(4) === snap1; #THROWS IndexOutOfBoundsException
     *   kortit.lisaa(snap1); kortit.getLkm() === 5;
     *   kortit.lisaa(snap1); kortit.getLkm() === 6;
     *   kortit.lisaa(snap1); kortit.getLkm() === 7;
     *   kortit.lisaa(snap1); kortit.getLkm() === 8;
     *   kortit.lisaa(snap1); #THROWS SailoException
     * 
     * </pre>
     */
    public void lisaa(Kortti kortti) throws SailoException {
	if (lkm >= alkiot.length)
	    alkiot = kasvataTaulukko();
	alkiot[lkm] = kortti;
	lkm++;
	muutettu = true;
    }

    /**
     * Kasvatetaan taulukkoa jos se meinaa k�yd� liian pieneksi
     * @return suurempi taulukko, jossa tyhj� tila lopussa
     */
    private Kortti[] kasvataTaulukko() {
	int kasvatus = 5;
	Kortti[] uusi = new Kortti[alkiot.length + kasvatus];
	for (int i = 0; i < alkiot.length; i++) {
	    uusi[i] = alkiot[i];
	}
	return uusi;
    }

    /**
     * Palautetaan halutussa paikassa oleva Kortti
     * @param paikka Kortin indeksi
     * @return haluttu Kortti
     * @throws IndexOutOfBoundsException virhe, jos indeksill� ei l�ydy korttia taulukosta
     */
    public Kortti anna(int paikka) throws IndexOutOfBoundsException {
	if (paikka < 0 || paikka >= lkm)
	    throw new IndexOutOfBoundsException("luvaton indeksi > " + paikka);
	return alkiot[paikka];
    }

    /**
     * Luetaan korttien tiedot tiedostosta
     * @param tiedNimi halutun tiedoston nimi
     * @throws SailoException  jos ei onnistu
     */
    public void lueTiedosto(String tiedNimi) throws SailoException {
	try (BufferedReader fi = new BufferedReader(new FileReader("kortit.dat"))) {
	    String rivi;
	    while ((rivi = fi.readLine()) != null) {
		rivi = rivi.trim();
		Kortti kortti = new Kortti();
		kortti.parse(rivi);
		lisaa(kortti);
	    }
	    muutettu = false;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Tallennetaan muutetut tiedot tiedostoon
     * @throws SailoException talletus ep�onnistui
     */
    public void talleta() throws SailoException {
	if (!muutettu)
	    return;
	File ftied = new File("kortit.dat");
	try (PrintWriter fo = new PrintWriter(new FileWriter(ftied.getCanonicalPath()))) {
	    for (int i = 0; i < getLkm(); i++) {
		Kortti kortti = anna(i);

		fo.println(kortti.toString());
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * 
     * @param nimi tiedoston nimi
     */
    public void setTiedostoNimi(String nimi) {
	tiedostoNimi = nimi;
    }

    /**
     * 
     * @return tiedoston nimi
     */
    public String getTiedostonNimi() {
	return tiedostoNimi + ".dat";
    }

    /**
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	Kortit kortit = new Kortit();

	Kortti snap1 = new Kortti(), snap2 = new Kortti();
	snap1.rekisteroi();
	snap2.rekisteroi();
	snap1.vastaaSnap();
	snap2.vastaaSnap();

	try {
	    kortit.lisaa(snap1);
	    kortit.lisaa(snap2);

	    for (int i = 0; i < kortit.getLkm(); i++) {
		Kortti kortti = kortit.anna(i);
		System.out.println("kortti nro > " + i);
		kortti.tulosta(System.out);
	    }

	} catch (SailoException e) {
	    System.out.println(e.getMessage());
	}

    }

    /**
     * @param haku merkkijono mille haetaan vastaavuuksia
     * @param numero kortin tunnusnumero
     * @return lista l�ytyneist� korteista
     */
    public Collection<Kortti> etsi(String haku, int numero) {
	List<Kortti> loytyneet = new ArrayList<Kortti>();
	for (int i = 0; i < numero; i++) {
	    Kortti kor = alkiot[i];
	    String j = kor.getNimi();
	    if (WildChars.onkoSamat(j, haku))
		loytyneet.add(kor);
	}
	return loytyneet;
    }

}
