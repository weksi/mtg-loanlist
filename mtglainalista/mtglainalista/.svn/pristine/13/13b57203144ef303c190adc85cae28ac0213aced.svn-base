/**
 * 
 */
package Lainalista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Osaa v�hent�� ja lis�t� lainauksia            
 * osaa lukea ja kirjoittaa Lainaukset.dat tiedos 
 * toon                                             
 * osaa etsi� ja lajitella        		
 * 
 * @author Ville-Veikko V�h�aho ja Mika koskela
 * @version 26.03.2014
 */
public class Lainaukset {
    private String tiedostonNimi = "";
    private final List<Lainaus> lainaukset = new ArrayList<Lainaus>();
    private boolean muutettu = false;
    private int lkm = 0;

    /**
    * Oletusmuodostaja, jossa ei viel� tehd� mit��n
    */
    public Lainaukset() {
	// ei tee mit��n viel�
    }

    /**
     * Haetaan kaikki yhden henkil�n lainaamat kortit
     * @param haltijaId Lainaajan tunnusNro
     * @return tietorakenne, jossa viitteet l�ydettyihin harrastuksiin
     * @example
     * <pre name="test">
     * #import java.util.*;
     * 
     * Lainaukset lainaukset = new Lainaukset();
     * Lainaus eka = new Lainaus(); 
     * Lainaus toka = new Lainaus(); 
     * Lainaus kolmas = new Lainaus(); 
     * Lainaus neljas = new Lainaus(); 
     * eka.vastaaApuLainaus(1);
     * toka.vastaaApuLainaus(2);
     * kolmas.vastaaApuLainaus(3);
     * neljas.vastaaApuLainaus(2);
     * lainaukset.lisaa(eka);        
     * lainaukset.lisaa(toka);      
     * lainaukset.lisaa(kolmas);  
     * lainaukset.lisaa(neljas);  
     * 
     * List<Lainaus> loytyneet;
     * loytyneet = lainaukset.annaLainaukset(1);
     * loytyneet.size() === 1;
     * loytyneet = lainaukset.annaLainaukset(2);
     * loytyneet.size() === 2;
     * loytyneet.get(0) == toka === true;
     * loytyneet.get(1) == neljas === true;
     * loytyneet = lainaukset.annaLainaukset(3);
     * loytyneet.size() === 1;
     * loytyneet.get(0) == kolmas === true;
     * loytyneet.get(0) == toka === false;
     * </pre>
     */
    public List<Lainaus> annaLainaukset(int haltijaId) {
	List<Lainaus> loydetyt = new ArrayList<Lainaus>();
	for (Lainaus lainaus : lainaukset) {
	    if (lainaus.getHaltijaId() == haltijaId) {
		loydetyt.add(lainaus);
	    }
	}
	return loydetyt;
    }

    /**
     * Haetaan kaikki yhdelt� henkil�lt� pois lainatut kortit 
     * @param hloId omistajan tunnusNro
     * @return lista l�ydetyist� lainauksista
     */
    public List<Lainaus> annaPoisLainatut(int hloId) {
	List<Lainaus> loydetyt = new ArrayList<Lainaus>();
	for (Lainaus lainaus : lainaukset) {
	    if (lainaus.getOmistajaId() == hloId) {
		loydetyt.add(lainaus);
	    }
	}
	return loydetyt;
    }

    /**
     * 
     * @return lainaukseen kuuluvien korttien lukum��r�
     */
    public int getLkm() {
	return lkm;
    }

    /**
     * Lis�t��n uusi lainaus tietorakenteeseen
     * @param lainaus lainaus olio
     * @example
     */
    public void lisaa(Lainaus lainaus) {
	lainaukset.add(lainaus);
	lkm++;
	muutettu = true;
    }

    /**
     * Luetaan tiedostosta tiedot, mutta ei osata viel�
     * @param tiedNimi tiedostonnimi, mist� luetaan
     * @throws SailoException poikkeus, jos tiedoston lukeminen ei onnistu
     */
    public void lueTiedosto(String tiedNimi) throws SailoException {
	// BufferedReader fi;
	try (BufferedReader fi = new BufferedReader(new FileReader("lainaus.dat"))) {
	    // fi = new BufferedReader(new FileReader("lainaus.dat"));
	    String rivi;
	    while ((rivi = fi.readLine()) != null) {
		rivi = rivi.trim();
		Lainaus lainaus = new Lainaus();
		lainaus.parse(rivi);
		lisaa(lainaus);
	    }
	    muutettu = false;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Talletetaan tiedostoon, mutta viel� ei osata
     * @throws SailoException jos tallentaminen ei onnistu niin heitet��n virhe
     */
    public void talleta() throws SailoException {
	if (!muutettu)
	    return;
	File ftied = new File("lainaus.dat");
	try (PrintWriter fo = new PrintWriter(new FileWriter(ftied.getCanonicalPath()))) {
	    for (Lainaus lainaus : lainaukset) {
		fo.println(lainaus.toString());
	    }
	    /*for (int i = 0; i < getLkm(); i++) {
		Lainaus lainaus = lainaukset.get(i);

		fo.println(lainaus.toString());
	    }*/
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	Lainaukset lista = new Lainaukset();
	Lainaus eka = new Lainaus();
	Lainaus toka = new Lainaus();
	eka.vastaaApuLainaus(1);
	toka.vastaaApuLainaus(2);
	lista.lisaa(eka);
	eka.rekisteroi();
	toka.rekisteroi();
	lista.lisaa(toka);

	List<Lainaus> lista2 = lista.annaLainaukset(1);
	for (Lainaus lainaus : lista2) {
	    lainaus.tulosta(System.out);

	}

    }

    /**
     * Selvitet��n, ett� l�ytyyk� halutusta kortista lainaoja halutulta henkil�lt� pois
     * @param hloId omistajan tunnusNro
     * @param id kortin tunnusNro
     * @return lainattujen korttien m��r�
     */
    public int annaHalututLainat(int hloId, int id) {
	int lainassa = 0;
	for (Lainaus lainaus : lainaukset) {
	    if (lainaus.getOmistajaId() == hloId && lainaus.getKorttiId() == id)
		lainassa += lainaus.getKpl();
	}
	return lainassa;
    }

    /**
     * Poistetaan lainaus
     * @param poistettava poistettava lainaus
     */
    public void poista(Lainaus poistettava) {
	lainaukset.remove(poistettava);
	muutettu = true;
    }

}
