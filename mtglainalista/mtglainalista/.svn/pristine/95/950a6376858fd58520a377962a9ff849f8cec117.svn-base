/**
 * 
 */
package Lainalista;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Ville-Veikko V�h�aho ja Mika Koskela
 *
 */
public class Tyypit {
    private static final int MAX_TYYPPIA = 10;
    private int tyyppienLkm = 0;
    private String tiedostonNimi = "";
    private Tyyppi[] alkiot = new Tyyppi[MAX_TYYPPIA];

    /**
     * oletus muodostaja
     */
    public Tyypit() {

    }

    /**
     * Lis�� uuden tyypin tietorakenteeseen. 
     * @param tyyppi lis�tt�v�n j�senen viite. tietorakenne muuttuu omistajaksi.
     * @throws SailoException jos tietorakenteeseen ei mahdu enemp�� alkioita
     * @example
     * <pre name="test">
     * #THROWS SailoException 
     * Tyypit tyypit = new Tyypit();
     * Tyyppi instant = new Tyyppi();
     * Tyyppi creature = new Tyyppi();
     * tyypit.getLkm() === 0;
     * tyypit.lisaa(instant); tyypit.getLkm()  === 1;
     * tyypit.lisaa(creature); tyypit.getLkm() === 2;
     * tyypit.lisaa(creature); tyypit.getLkm() === 3;
     * tyypit.anna(0) === instant;
     * tyypit.anna(1) === creature;
     * tyypit.anna(2) === creature;
     * tyypit.anna(1) == instant=== false;
     * tyypit.anna(0) == instant === true;
     * tyypit.anna(1) == creature === true;
     * tyypit.anna(3) === instant; #THROWS IndexOutOfBoundsException	
     * tyypit.lisaa(creature); tyypit.getLkm() === 4;
     * tyypit.lisaa(instant); tyypit.getLkm()  === 5;
     * tyypit.lisaa(creature); tyypit.getLkm() === 6;
     * tyypit.lisaa(instant); tyypit.getLkm(); #THROWS SailoException
     * </pre>
     */
    public void lisaa(Tyyppi tyyppi) throws SailoException {
	if (tyyppienLkm >= alkiot.length)
	    throw new SailoException("tila loppui");
	alkiot[tyyppienLkm] = tyyppi;
	tyyppienLkm++;

    }

    /**
     * palauttaa viitteen henkil�n paikasta i
     * @param i mink� paikan tyyppi halutaan
     * @return viite tyyppiin, jonka indeksi on i
     * @throws IndexOutOfBoundsException jos i ei ole sallitulla alueella 
     */
    public Tyyppi anna(int i) throws IndexOutOfBoundsException {

	if (i < 0 || tyyppienLkm <= i)
	    throw new IndexOutOfBoundsException("Laitonindeksi: " + i);
	return alkiot[i];
    }

    /**
     * Palauttaa tyyppi rekisterin tyyppien lukum��r�n
     * @return tyyppien lukum��r�
     */
    public int getLkm() {
	return tyyppienLkm;
    }

    /**
     * Lukee tyyppi tiedostoa. T�m� osa viel� kesken.
     * @param hakemisto tiedoston hakemisto
     * @throws SailoException jos lukeminen ep� onnistuu
     *     
     */
    public void lueTiedostosta(String hakemisto) throws SailoException {
	// tiedostonNimi = hakemisto + "/tyypit.dat";
	// throw new SailoException("Ei osata viel� luke tiedosta" + tiedostonNimi);
	// BufferedReader fi = null;
	try (BufferedReader fi = new BufferedReader(new FileReader("tyypit.dat"))) {
	    // fi = new BufferedReader(new FileReader("Tyypit.dat"));
	    String rivi = " ";
	    while ((rivi = fi.readLine()) != null) {
		rivi = rivi.trim();
		Tyyppi tyyppi = new Tyyppi();
		tyyppi.parse(rivi);
		lisaa(tyyppi);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}

	/*
	 * finally { try { fi.close(); } catch (IOException e) { e.printStackTrace(); } }
	 */

    }

    /**
     * Tallentaa tyypit tiedostoon.  T�m� osa on viel� kesken.
     * @throws SailoException jos tallennus ep�onnistuu
     */
    public void tallenna() throws SailoException {
	//throw new SailoException("Ei osata viel� tallettaa tiedostoa " + tiedostonNimi);
    }

    /**
     * @param args ei k�yt�ss�
     */
    public static void main(String[] args) {
	Tyypit tyypit = new Tyypit();

	Tyyppi instant = new Tyyppi();
	instant.rekisteroi();
	instant.vastaaTyyppi2();
	Tyyppi creature = new Tyyppi();
	creature.rekisteroi();
	creature.vastaaTyyppi();

	try {
	    tyypit.lisaa(instant);
	    tyypit.lisaa(creature);

	    System.out.println("Tyypit testi");

	    for (int i = 0; i < tyypit.getLkm(); i++) {
		Tyyppi tyyppi = tyypit.anna(i);
		System.out.println("tyyppi nro: " + i);
		tyyppi.tulosta(System.out);

	    }
	} catch (SailoException ex) {
	    System.out.println(ex.getMessage());
	}

    }
}
