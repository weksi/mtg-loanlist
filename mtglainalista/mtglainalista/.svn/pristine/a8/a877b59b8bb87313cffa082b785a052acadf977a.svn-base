package Lainalista;

import java.io.OutputStream;
import java.io.PrintStream;

import kanta.Apuja;

import fi.jyu.mit.ohj2.Mjonot;

/**
 * @author Ville-Veikko V�h�aho ja Mika Koskela
 * @version 1.0, 8.3.2014
 * 
 * Tiet�� henkil�rekisterin kent�t(hloid, nimi)
 * osaa tarkistaa tietyn kent�n oikeellisuuden
 * osaaa muuttaa 1|Weksi| merkkijonon henkil�n
 * tiedoksi
 */
public class Henkilo {

    private String nimi = "";
    private int hloid;
    private static int seuraavaId = 1;

    /**
     * Oletusmuodostaja testausta varten
     */
    public Henkilo() {
	nimi = "";
	hloid = 0;
    }

    /**
     * Muodostaja, jossa saadaan nimi parametrin�
     * @param nimi Henkil�lle annettava nimi
     */
    public Henkilo(String nimi) {
	this.nimi = nimi;
	hloid = 0;
    }

    /**
     * Alustetaan henkil�n tiedot annetuilal arvoilla
     * @param nimi henkil�n uusi nimi
     */

    /**
     * Apumetodi, jolla saadaan t�ytetty� testiarcot henkil�lle.
     */
    public void vastaaHenkilo() {
	nimi = "Mika" + (int) Apuja.rand(100, 999);
    }

    /**
     * Apumetodi, jolla saadaan t�ytetty� testiarcot henkil�lle.
     */
    public void vastaaHenkilo2() {
	nimi = "Weksi";

    }

    /**
     * Palauttaa henkil�n nimen
     * @return palautetaan henkil�n nimi
     * @example
     * <pre name="test">
     * Henkilo mika = new Henkilo(); 
     * mika.vastaaHenkilo(); 
     * mika.getNimi()  === "Mika";
     * </pre>
     */
    public String getNimi() {
	return (nimi);

    }

    /**
     * palauttaa henkil�n id:n
     * @return henkil�n id
     */
    public int getHloid() {
	return (hloid);
    }

    private void setHloid(int nr) {
	hloid = nr;
	if (hloid >= seuraavaId)
	    seuraavaId = hloid + 1;
    }

    /**
     * Tulostetaan henkil�n tiedot
     * @param tvirta tietovirta, jonne tiedot tulostetaan
     * @example
     * <pre name="test">
     * #import java.io.*; 
     * ByteArrayOutputStream byteoutput = new ByteArrayOutputStream(); 
     * Henkilo weksi = new Henkilo(); 
     * weksi.vastaaHenkilo2(); 
     * weksi.tulosta(byteoutput);
     * </pre>
     */
    public void tulosta(OutputStream tvirta) {
	@SuppressWarnings("resource")
	PrintStream out = tvirta instanceof PrintStream ? (PrintStream) tvirta : new PrintStream(
		tvirta);
	out.println(hloid + " " + nimi);

    }

    /**
     * Antaa j�senelle henkil� id:n
     * @return j�senen uusi henkil� id
     * @example
     * <pre name="test">
     * Henkilo weksi = new Henkilo(); 
     * weksi.vastaaHenkilo2(); 
     * weksi.getHloid() === 0; 
     * weksi.rekisteroi(); 
     * Henkilo mika = new Henkilo(); 
     * mika.vastaaHenkilo(); 
     * mika.rekisteroi(); 
     * int n1 = weksi.getHloid(); 
     * int n2 = mika.getHloid(); 
     * n1 === n2-1;
     * </pre>
     */
    public int rekisteroi() {
	hloid = seuraavaId;
	seuraavaId++;
	return hloid;

    }

    /**
     * 
     * @param rivi tiedoston rivi mist� olion tiedot poimitaan
     */
    public void parse(String rivi) {
	StringBuffer sb = new StringBuffer(rivi);
	setHloid(Mjonot.erota(sb, '|', getHloid()));
	nimi = Mjonot.erota(sb, '|', nimi);
    }

    /**
     * 
     */
    @Override
    public String toString() {
	return "" + getHloid() + "|" + nimi;

    }

    /**
     * testaan henkil� luokkaa
     * @param args ei k�yt�ss�
     */

    public static void main(String[] args) {

	Henkilo weksi = new Henkilo();
	weksi.rekisteroi();
	weksi.vastaaHenkilo2();
	Henkilo mika = new Henkilo();
	mika.rekisteroi();
	mika.vastaaHenkilo();
	weksi.tulosta(System.out);
	mika.tulosta(System.out);
    }

}